module wikipedia-xml-parser

go 1.15

require (
	github.com/mattn/go-sqlite3 v1.14.8
	github.com/wcharczuk/go-chart/v2 v2.1.0
)
